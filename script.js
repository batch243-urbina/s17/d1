// console.log("Hello Worlddddddddddddddddddddddddd!");

// FUNCTIONS

function printName() {
  console.log(`My name is EJ`);
}
printName();

function declaredFunction() {
  console.log(`Hello from declaredFunction!`);
}
declaredFunction();

let variableFunction = function () {
  console.log(`Hello from variableFunction!`);
};
variableFunction();

let funcExpression = function funcName() {
  console.log(`Hello from funcExpression!`);
};
funcExpression();

declaredFunction = function () {
  console.log(`Updated declaredFunction`);
};
declaredFunction();

const constantFunc = function () {
  console.log(`Initialized with const!`);
};
constantFunc();

// Function scoping
// accessibility of variable within our program
// local
// global
// function

{
  let localVar = "EJ Urbina";
  console.log(localVar);
}

let globalVar = "Mr. Worldwide";

function showNames() {
  let functionLet = "Jane";
  const functionConst = "John";

  console.log(functionConst);
  console.log(functionLet);
}

// showNames();
// console.log(functionConst);
// console.log(functionLet);

function myNewFunction() {
  let name = "Jane";
  console.log(name);

  function nestedFunction() {
    let nestedName = "John";

    console.log(nestedName);
    console.log(name);
  }
  nestedFunction();
}

myNewFunction();

// Function and Global Scope Variables

let globalName = "Alexandro";
function myNewFunction2() {
  let nameInside = "Renz";
  console.log(globalName);
  console.log(nameInside);
}
myNewFunction2();

// Alert: alert();
// allows us to show a small window at the top of browser
// short dialog/messages
// alert(`Hello World!`);

// function showSampleAlert() {
//   alert(`Hello, User!`);
// }
// showSampleAlert();

// Prompt()
// gather user input
let samplePrompt = prompt(`Enter your name`);
console.log(samplePrompt);
console.log(typeof samplePrompt);

function printWelcomeMessages() {
  let firstName = prompt(`Enter your First Name: `);
  let lastName = prompt(`Enter your Last Name: `);
  console.log(firstName);
  console.log(lastName);
  console.log(`Hello, ${firstName} ${lastName}!`);
}
printWelcomeMessages();

// Function Naming Conventions
// Descriptive of task it will perform
// Avoid generic names
// Avoid pointless names
// Use camel casing

function getCourses() {
  let course = ["Science 101", "Math 101", "English 101"];
  console.log(course);
}
getCourses();

function displayCarInfo() {
  console.log(`Brand: Toyota`);
}

displayCarInfo();
